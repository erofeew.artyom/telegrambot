﻿using Microsoft.EntityFrameworkCore;

namespace TelegramBot
{
    internal class Program
    {
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        static void Main(string[] args)
        {
            try
            {
                // Инициализация базы данных
                using (var db = new Storage.BotDbContext())
                {
                    db.Database.Migrate();
                    foreach (var user in db.Users)
                    {
                        log.Trace(user);
                    }
                }
                log.Info("База данных инициализирована");

                var bot = new TeleBot();
                bot.Start();
                Console.WriteLine("Нажмите Enter для завершения");
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                log.Fatal(ex);
            }
        }
    }
}
