﻿using Microsoft.EntityFrameworkCore;
using NLog;
using System.Linq;
using System.Text;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramBot.Extension;
using TelegramBot.Storage;


namespace TelegramBot
{
    internal class BotHandler : Telegram.Bot.Polling.IUpdateHandler
    {
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        private const string textRegister = "Регистрация без номера телефона";
        // Метод для обработки команды
        delegate void CommandHandler(ITelegramBotClient botClient, Message message);
        private static Dictionary<string, CommandHandler> commands = new()
        {
            {"register", RegisterCommand },
            {"start", StartCommand },
            { "forget", ForgetCommand },
            { "profile", ProfileCommand },
            { "courses", ViewCoursesCommand},
            { "addcourses", AddCourseCommand },
            { "deletecourse", DeleteCourse },
            { "enroll", EnrollCommand },

        };

        public Task HandlePollingErrorAsync(ITelegramBotClient botClient, Exception exception, CancellationToken cancellationToken)
        {
            log.Error(exception);
            return Task.CompletedTask;
        }

        public Task HandleUpdateAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
        {
            try
            {
                switch (update.Type)
                {
                    case Telegram.Bot.Types.Enums.UpdateType.Message:
                        ProcessMessage(botClient, update.Message);
                        break;

                    default:
                        botClient.Send(update.Message.From.Id, LogLevel.Warn, $"Обновления типа {update.Type} пока не поддерживаются");
                        break;
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            return Task.CompletedTask;

        }


        private async Task ProcessMessage(ITelegramBotClient botClient, Message message)
        {
            using (var context = new Storage.BotDbContext())
            {             
                Storage.User? user = await context.Users.FirstOrDefaultAsync(x => x.Id == message.From.Id);
                if (user != null)
                {
                    user.LastActivity = DateTime.Now;
                    context.SaveChanges();
                }
            }
            switch (message.Type)
            {
                case Telegram.Bot.Types.Enums.MessageType.Text:
                    if (message.Text[0] == '/')
                    {
                        ProcessCommand(botClient, message);
                    }
                    else
                    {
                        ProcessText(botClient, message);
                    }
                    break;

                case Telegram.Bot.Types.Enums.MessageType.Contact:
                    ProcessContact(botClient, message);
                    break;

                default:
                    botClient.Send(message.From.Id, LogLevel.Warn, $"Сообщения типа {message.Type} пока не поддерживаются");
                    break;
            }
        }
        private void ProcessText(ITelegramBotClient botClient, Message message)
        {
            using var db = new Storage.BotDbContext();
            Storage.User? user = db.Users.Find(message.From.Id);
            switch (user?.State)
            {
                case Enums.State.Register:
                    if (message.Text == textRegister)
                    {
                        botClient.Send(message.From.Id, LogLevel.Trace, $"Спасибо за регистрацию!");
                        user.Registered = true;

                    }
                    break;

                case Enums.State.AddCourses:

                    if (!string.IsNullOrEmpty(message.Text))
                    {
                        var newCourse = new Course { Name = message.Text };
                        db.Courses.Add(newCourse);
                        db.SaveChanges();
                        botClient.Send(message.From.Id, LogLevel.Trace, $"Курс '{message.Text}' успешно добавлен.");
                        user.State = Enums.State.General; // Переводим пользователя обратно в общее состояние
                        db.SaveChanges();

                    }
                    else
                    {
                        botClient.Send(message.From.Id, LogLevel.Warn, "Название курса не может быть пустым.");
                    }
                    break;
                case Enums.State.Enroll:
                    if (!string.IsNullOrEmpty(message.Text))
                    {
                        // Проверяем, есть ли курс с таким названием в базе данных
                        var existingCourse = db.Courses.FirstOrDefault(c => c.Name == message.Text);
                        if (existingCourse != null)
                        {
                            // Создаем запись о курсе в базе данных
                            db.UsersCourses.Add(new UsersCourses
                            {
                                UserId = user.Id,
                                CourseId = existingCourse.Id
                            });
                            db.SaveChanges();

                            // Отправляем сообщение об успешной записи на курс
                            botClient.Send(message.From.Id, LogLevel.Trace, $"Вы успешно записались на курс: {message.Text}");

                            // Переводим пользователя обратно в общее состояние
                            user.State = Enums.State.General;
                            db.SaveChanges();
                        }
                        else
                        {
                            // Если курс с таким названием не найден, отправляем сообщение об ошибке
                            botClient.Send(message.From.Id, LogLevel.Warn, $"Курс '{message.Text}' не найден.");
                        }
                    }
                    else
                    {
                        // Если пользователь не ввел название курса, отправляем сообщение об ошибке
                        botClient.Send(message.From.Id, LogLevel.Warn, "Название курса не может быть пустым.");
                    }
                    break;


                default:
                    botClient.Send(message.From.Id, LogLevel.Trace, $"Вы прислали мне: {message.Text}");
                    break;

            }
        }
        private void ProcessContact(ITelegramBotClient botClient, Message message)
        {
            using var db = new Storage.BotDbContext();
            Storage.User user = db.Users.Find(message.From.Id);
            if (user?.State != Enums.State.Register)
            {
                botClient.Send(message.From.Id, LogLevel.Trace, $"Вы зачем-то прислали мне номер телефона");
                return;
            }
            if (message.From.Id != message.Contact.UserId)
            {
                botClient.Send(message.From.Id, LogLevel.Debug, $"Вы прислали мне не свой номер телефона");
                return;
            }
            botClient.Send(message.From.Id, LogLevel.Trace, $"Вы зарегистрированы с номером телефона {message.Contact.PhoneNumber}");
            user.Phone = message.Contact.PhoneNumber;
            user.State = Enums.State.General;
            user.Registered = true;
            db.SaveChanges();
        }


        private void ProcessCommand(ITelegramBotClient botClient, Message message)
        {
            string command = message.Text[1..].ToLower();
            if (commands.TryGetValue(command, out CommandHandler? handler))
            {
                handler(botClient, message);
            }
            else
            {
                botClient.Send(message.From.Id, LogLevel.Warn, $"Команда типа {command} не поддерживается");
            }
        }
        private static void StartCommand(ITelegramBotClient botClient, Message message)
        {
            botClient.Send(message.From.Id, LogLevel.Trace, $"Здравствуйте, {message.From.FirstName}");
        }
        private static void ForgetCommand(ITelegramBotClient botClient, Message message)
        {
            using var db = new Storage.BotDbContext();
            Storage.User? user = db.Users.Find(message.From.Id);
            if (user == null)
            {
                botClient.Send(message.From.Id, LogLevel.Trace, $"Вы не зарегистрированы");
                return;
            }

            db.Users.Remove(user);
            db.SaveChanges();
            botClient.Send(message.From.Id, LogLevel.Trace, $"Регистрационные данные удалены");
        }

        private static void RegisterCommand(ITelegramBotClient botClient, Message message)
        {
            using var db = new Storage.BotDbContext();
            Storage.User? user = db.Users.Find(message.From.Id);
            if (user != null && user.Registered)
            {
                botClient.Send(message.From.Id, LogLevel.Trace, $"{user.UserName} Вы уже зарегистрированы");
                return;
            }

            var isFirstUserAsAdmin = bool.Parse(Statics.Config.GetParameter("FirstUserAsAdmin") ?? "false");

            if (isFirstUserAsAdmin && !db.Users.Any())
            {
                user = new Storage.User()
                {
                    Id = message.From.Id,
                    FirstName = message.From.FirstName,
                    LastName = message.From.LastName,
                    UserName = message.From.Username,
                    Registered = true, // Помечаем первого пользователя как зарегистрированного
                    isAdmin = true // Помечаем первого пользователя как администратора
                };

                db.Users.Add(user);
            }
            else
            {
                // Обычная регистрация без назначения администратора
                if (user == null)
                {
                    user = new Storage.User()
                    {
                        Id = message.From.Id,
                        FirstName = message.From.FirstName,
                        LastName = message.From.LastName,
                        UserName = message.From.Username
                    };
                    db.Users.Add(user);
                }
            }

            user.State = Enums.State.Register;
            db.SaveChanges();

            var keys = new KeyboardButton[2]
            {
        new KeyboardButton(textRegister),
        new KeyboardButton("Регистрация по номеру телефона")
        {
            RequestContact = true
        }
            };
            var markup = new ReplyKeyboardMarkup(keys)
            {
                OneTimeKeyboard = true,
                ResizeKeyboard = true
            };
            botClient.SendTextMessageAsync(message.From.Id, "Подтвердите регистрацию", replyMarkup: markup);
        }


        private static void ProfileCommand(ITelegramBotClient botClient, Message message)
        {
            using var db = new Storage.BotDbContext();

            Storage.User? user = db.Users.Find(message.From.Id);

            if (user?.Registered ?? false)
            {
                user.FirstName = message.From.FirstName;
                user.LastName = message.From.LastName;
                user.UserName = message.From.Username;
                db.SaveChanges();

                botClient.Send(message.From.Id, LogLevel.Trace, $"Ваш профиль: {user.FirstName} {user.LastName} @{user.UserName}, телефон {user.Phone}");
            }
        }

        private static async void ViewCoursesCommand(ITelegramBotClient botClient, Message message)
        {
            using var db = new Storage.BotDbContext();
            var courses = await db.Courses.OrderBy(x => x.Name).ToListAsync();
            Storage.User user = await db.Users.FindAsync(message.From.Id);

            if (courses.Any())
            {
                var coursesList = new StringBuilder();
                foreach (var course in courses)
                {
                    coursesList.AppendLine($" {course.Name}");
                }
                botClient.Send(message.From.Id, LogLevel.Trace, $"Список учебных курсов:{coursesList.ToString()}");

            }
            else
            {
                botClient.Send(message.From.Id, LogLevel.Trace, "Список учебных курсов пуст.");

            }
            user.State = Enums.State.General;
        }
        private static async void AddCourseCommand(ITelegramBotClient botClient, Message message)
        {
            using var db = new Storage.BotDbContext();

            Storage.User? user = await db.Users.FindAsync(message.From.Id);

            if (user == null || !user.isAdmin)
            {
                botClient.Send(message.From.Id, LogLevel.Warn, "Только администратор может добавить новый курс.");
                return;
            }

            botClient.Send(message.From.Id, LogLevel.Trace, $"Введите название курса: ");
            user.State = Enums.State.AddCourses; // Переводим пользователя в состояние AddCourses
            db.SaveChanges();

        }
        private static void DeleteCourse(ITelegramBotClient botClient, Message message)
        {
            using var db = new Storage.BotDbContext();
            Storage.User user = db.Users.Find(message.From.Id);
            Storage.Course? course = db.Courses.FirstOrDefault();
            if (user == null || !user.isAdmin)
            {
                botClient.Send(message.From.Id, LogLevel.Warn, "Только администратор может удалить курс.");
                return;
            }
            if (course == null)
            {
                botClient.Send(message.From.Id, LogLevel.Trace, "Такого курса нет");
            }
            else
            {
                db.Courses.Remove(course);
            }
            db.SaveChanges();
            botClient.Send(message.From.Id, LogLevel.Trace, $"Курс {course.Name} удален");
            user.State = Enums.State.General;
        }
        private static void EnrollCommand(ITelegramBotClient botClient, Message message)
        {
            using var db = new Storage.BotDbContext();
            // Получаем пользователя из базы данных
            var user = db.Users.Find(message.From.Id);
            if (user == null || !user.Registered)
            {
                // Если пользователь не зарегистрирован, отправляем сообщение о необходимости сначала зарегистрироваться
                botClient.Send(message.From.Id, LogLevel.Warn, "Для записи на курс необходимо сначала зарегистрироваться.");
                return;
            }

            botClient.Send(message.From.Id, LogLevel.Trace, $"Введите название курса: ");
            user.State = Enums.State.Enroll;
            db.SaveChanges();
        }
    }
}

