﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelegramBot.Statics
{
    public static class Config
    {
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        private static readonly IConfigurationRoot config;
        

        static Config()
        {
            string name = Path.Combine(AppContext.BaseDirectory, "appsettings.json");
            if (!System.IO.File.Exists(name))
            {
                log.Fatal($"Файл конфигурации не найден: {name}");
                Environment.Exit(1);
            }
            config = new ConfigurationBuilder().AddJsonFile(name, false, true).Build();
        }
        public static string? GetParameter(string name)
        {
            return config.GetSection(name).Value;
        }
      
    }
}
