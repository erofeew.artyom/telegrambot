﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;

namespace TelegramBot.Extension
{
    internal static class BotExtension
    {
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        internal static void Send(this ITelegramBotClient botClient,long id, NLog.LogLevel level, string message)
        {
            botClient.SendTextMessageAsync(id, message);
            log.Log(level, message);
        }
    }
}
