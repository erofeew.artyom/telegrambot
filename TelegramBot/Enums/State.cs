﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelegramBot.Enums
{
    public enum State
    {  
        //Основное состояние
        General,
        // Регистрация нового пользователя
        Register,
        // Ожидание добавление курса 
        AddCourses,
        // Ожидание записи на курс
        Enroll


    }
}
