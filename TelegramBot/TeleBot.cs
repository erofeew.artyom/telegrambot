﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Polling;
using TelegramBot.Storage;

namespace TelegramBot
{
    internal class TeleBot
    {
        private static readonly NLog.Logger log = NLog.LogManager.GetCurrentClassLogger();
        private readonly TelegramBotClient client;
        private readonly BotHandler handler;

        internal TeleBot()
        {        
            string token = Statics.Config.GetParameter("Token");
            client = new Telegram.Bot.TelegramBotClient(token ?? string.Empty);
            handler = new();
        }
        
        internal void Start()
        {
           
            if (client.TestApiAsync().Result)
            {
                log.Info("Соединение установлено");
            }
            else
            {
                log.Error("Соединение не установлено");
            }
            client.StartReceiving(handler);
        }
    }
}
