﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelegramBot.Storage
{
    public class UsersCourses
    {
        public int Id { get; set; }
        public long UserId { get; set; }
        public int CourseId { get; set; }
    }
}
