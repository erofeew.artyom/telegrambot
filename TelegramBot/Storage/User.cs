﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelegramBot.Storage
{
    public class User
    {  
        public long Id { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? UserName { get; set; }
        public string? Phone {  get; set; }
        public Enums.State State { get; set; } = Enums.State.General;
        

        // Зарегистрированный пользователь
        public bool Registered { get; set; }

        //Админ
        public bool isAdmin { get; set; }

        // Метка времени получения последнего сообщения от пользователя
        public DateTime LastActivity { get; set; } = DateTime.Now;

        public override string ToString()
            => $"{Id}: {UserName} {Registered}";
    }
    
}
