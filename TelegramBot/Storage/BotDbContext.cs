﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types;



namespace TelegramBot.Storage
{
    public class BotDbContext : DbContext
    {
        public DbSet<Course> Courses { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UsersCourses> UsersCourses { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // Указываем использовать SQLite базу данных
            optionsBuilder.UseSqlite("Data Source=bot.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Дополнительные настройки моделей или базы данных могут быть добавлены здесь
        }
    }
}


